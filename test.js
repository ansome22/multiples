//TODO
var helper = require("./helper.js");

var total = helper.start([5, 15, 30]);

//if corrent count
if ((total = 105)) {
  console.log("Whole Program Passed");
} else {
  console.log("Whole Program Failed");
}

//check if multiple will throw error
try {
  helper.multiple(31);
} catch (error) {
  // if range error, meaning iterations is to large
  if (error instanceof RangeError) {
    // label what task number the issue took place in
    console.log("Error Check Passed: ");
    console.log(error.message);
  } else {
    // label what task number the issue took place in
    console.log("Error Check Failed");
  }
}
