//test different types of storing a variable
var helper = require("./helper.js");
const { performance } = require("perf_hooks");
//iterate 1000 times
let count = 10000;
//numbers to go through
let source = [1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23, 24, 25, 26, 27, 28, 29, 30];
//test with an string variable
function concatAsString() {
  var str = "";
  for (var c = 0; c < count; c++) {
    for (var i = 0; i < source.length; i++) {
      str += source[i];
    }
  }
  return str;
}
//test with a new array variable
function arrayjoin() {
  var arr = new Array();
  for (var c = 0; c < count; c++) {
    for (var i = 0; i < source.length; i++) {
      arr.push(source[i]);
    }
  }
  return arr;
}
//test with an array variable
function concatAsArray() {
  var array = [];
  for (var c = 0; c < count; c++) {
    for (var i = 0; i < source.length; i++) {
      array.push(source[i]);
    }
  }
  return array.join("");
}
//test with a map variable
function mapjoin() {
  var arr = new Map();
  for (var c = 0; c < count; c++) {
    for (var i = 0; i < source.length; i++) {
      arr.set(c, source[i]);
    }
  }
  return arr;
}
//test with a new string variable
function concatAsTrueString() {
  var str = new String();
  for (var c = 0; c < count; c++) {
    for (var i = 0; i < source.length; i++) {
      str += source[i];
    }
  }
  return str;
}

function forMultiple() {
  // variable to store final result
  // choose map from my tests of 1000 iterations map handled storing the sequence the quickest
  let result = new Map();

  for (var c = 0; c < count; c++) {
    // a loop dependent on the iterations given in the function
    for (var i = 0; i < source.length; i++) {
      // if element is a multiple of 3 and 5 there will be no remainder for both
      // check that item is multiple of 3 and 5 is at the top so that this check take priority.
      if (source[i] % 3 === 0 && source[i] % 5 === 0) {
        result.set(c, "HeyHoo");
      }
      // if element is a multiple of 3 there will be no remainder
      else if (source[i] % 3 === 0) {
        result.set(c, "Hey");
        // if element  is a multiple of 5 there will be no remainder
      } else if (source[i] % 5 === 0) {
        result.set(c, "Hoo");
        //if it is not any of the multiples above
      } else {
        result.set(c, ">");
      }
    }
  }
  // print result for this task
  return result;
}

// ----------- TEST -----------------
function runTest() {
  console.log(`\n--------------------------`);
  console.log(`"concatAsString" started..`);
  var t0 = performance.now();
  var str1 = concatAsString();
  var t1 = performance.now();
  console.log(`"concatAsString" completed:`);
  console.log("Time 1: " + (t1 - t0) + " milliseconds.");

  console.log(`\n--------------------------`);
  console.log(`"concatAsArray" started..`);
  t0 = performance.now();
  var str2 = concatAsArray();
  t1 = performance.now();
  console.log(`"concatAsArray" completed:`);
  console.log("Time 2: " + (t1 - t0) + " milliseconds.");

  console.log(`\n--------------------------`);
  console.log(`"concatAsTrueString" started..`);
  t0 = performance.now();
  var str3 = concatAsTrueString();
  t1 = performance.now();
  console.log(`"concatAsTrueString" completed:`);
  console.log("Time 3: " + (t1 - t0) + " milliseconds.");

  console.log(`\n--------------------------`);
  console.log(`"arrayJoinAsTrueArray" started..`);
  t0 = performance.now();
  var str4 = arrayjoin().join("");
  t1 = performance.now();
  console.log(`"arrayJoinAsTrueArray" completed:`);
  console.log("Time 4: " + (t1 - t0) + " milliseconds.");

  console.log(`\n--------------------------`);
  console.log(`"mapJoinAsMap" started..`);
  t0 = performance.now();
  var map1 = mapjoin();
  var str5 = "";
  map1.forEach((value, key) => {
    str5 += value;
  });
  t1 = performance.now();
  console.log(`"mapJoinAsMap" completed:`);
  console.log("Time 5: " + (t1 - t0) + " milliseconds.");

  console.log(`\n--------------------------`);
  console.log(`"ForLoopStarted" started..`);
  t0 = performance.now();
  var str6 = forMultiple();
  t1 = performance.now();
  console.log(`"ForLoopStarted" completed:`);
  console.log("Time 6: " + (t1 - t0) + " milliseconds.");

  console.log(`\n--------------------------`);
  console.log(`"multiple" started..`);
  t0 = performance.now();
  helper.main([5, 15, 30]);
  t1 = performance.now();
  console.log(`"multiple" completed:`);
  console.log("Time 7: " + (t1 - t0) + " milliseconds.");
}

runTest();
