// function to test if sequence has multiples of 3, 5 and are both multiple of 3 and 5
var multiple = function(iterations) {
  // the sequence of numbers iterations will go over
  let sequence = [1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23, 24, 25, 26, 27, 28, 29, 30];

  // variable to store final result
  // choose map from my tests of 1000 iterations map handled storing the sequence the quickest
  let result = new Map();

  // If iteration number is larger than sequence then throw error and with the message iteration is too large and will skip task.
  if (iterations > sequence.length) {
    throw new RangeError("iteration is larger than sequence");
  }

  // a loop dependent on the iterations given in the function
  for (let i = 0; i < iterations; i++) {
    // if element is a multiple of 3 and 5 there will be no remainder for both
    // check that item is multiple of 3 and 5 is at the top so that this check take priority.
    if (sequence[i] % 3 === 0 && sequence[i] % 5 === 0) {
      result.set(i, "HeyHoo");
    }
    // if element is a multiple of 3 there will be no remainder
    else if (sequence[i] % 3 === 0) {
      result.set(i, "Hey");
      // if element  is a multiple of 5 there will be no remainder
    } else if (sequence[i] % 5 === 0) {
      result.set(i, "Hoo");
      //if it is not any of the multiples above
    } else {
      result.set(i, ">");
    }
  }
  //return the final result from the iterations
  return result;
};
// print result for this task
var print_task = async function(item, index) {
  //create blank string so mean joining we do not join a undefined
  var string = "";
  // loop though all items of this task
  item.forEach((value, key) => {
    string += value;
  });
  // label to be result with task number
  console.log("Task %s: %s", index + 1, string);
};

// function to print total /  print a variable Int with the label Count
var print_count = async function(item) {
  // label to be result with task number
  console.log("Count: %s", item);
};

//function to process the element given and return the processed elements characters length
var processelement = function(element, index) {
  let count = 0;
  // to ensure not encountering possible error
  // if we do we put it in a try clause so the program can continue running and we can and logic to handle the error
  try {
    // get item return to count characters
    let item = multiple(element);
    //go to print_task to print the processed result to user
    print_task(item, index);
    // loop through all items to be counted
    item.forEach(value => {
      count += value.length;
    });
    //return count to be added to total
    return count;
  } catch (error) {
    // if range error, meaning iterations is to large
    if (error instanceof RangeError) {
      // label what task number the issue took place in
      console.log("Task %s: %s", index + 1, error.name + ": " + error.message);
    } else {
      // label what task number the issue took place in
      console.log("Task %s: %s", index + 1, error);
    }
  }
};

// function focusing around processing array of iterations
var processarray = function(array, callback) {
  let total = 0;
  //loop over each iteration
  // this is so I do not have to call multiple function repeatedly in the code.
  array.forEach((element, index) => {
    total += processelement(element, index);
  });
  //total count result sent back to be printed
  callback(total);
};

var start = function(array) {
  // create space in log
  console.log();
  //being processing array with array given
  processarray(array, i => {
    //once callback is called assumingly to print total
    console.log("Total count: ", i);
    return i;
  });
};

// export functions for other scripts to use.
module.exports = { start, processarray, processelement, print_count, print_task, multiple };
